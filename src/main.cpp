#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>

#include <opencv2/opencv.hpp>

#include "generation.hpp"
#include "bot.hpp"

int set_random_seed()
{
	srand(time(0));
	int seed = rand();
	srand(seed);
	return seed; // 12439092, 39289046
}

int main()
{
	printf("Seed: %d\n", set_random_seed());

	Generation::EstimateFunc estimate_func = [=](Bot bot) {
		CompiledBot cbot = bot.compile(10, 10);
		return cbot.getExternals() + cbot.getRepetition() + cbot.getDistanceFrom(9, 9);
	};

	Generation gen = Generation::random_generation(1000, estimate_func, 66);

	cv::namedWindow("image");

	for (int i = 0; i < 1000; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			gen = gen.net_gen(1000, estimate_func);
			// return 0;
		}
		
		cv::imshow("image", gen.get_best_bot().compile(10, 10).toImage(50));
		cv::waitKey(33);
		printf("Score (%d): %f\n", i, gen.get_best_score());
	}
	printf("Score: %f\n", gen.get_best_score());

	return 0;
}
