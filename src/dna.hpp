#pragma once

#include <vector>
#include <cstdint>
#include <ostream>


class SimpleDNA
{
private:
	SimpleDNA(std::vector<uint32_t> data);

public:
	const static uint32_t MAX_VALUE = RAND_MAX;

	std::vector<uint32_t> data;

	SimpleDNA();
	SimpleDNA(int len);

	size_t size() const;

	SimpleDNA mutation(double k) const;
	SimpleDNA mutation_sum(double k, double min_p, double max_p) const;
	SimpleDNA crossover(const SimpleDNA& b) const;
	SimpleDNA crossover_points(const SimpleDNA& b, int max_steps) const;

	friend std::ostream& operator <<(std::ostream& out, const SimpleDNA& dna);
};
