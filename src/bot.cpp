#include "bot.hpp"

#include <algorithm>


Bot::Bot() {}

Bot::Bot(int path_len) : path(path_len) {}

Bot Bot::operator +(const Bot& bot) const
{
    Bot res;

    if (rand() % 10 == 0)
    {
        res.path = path.crossover(bot.path).mutation(0.1);
        return res;
    }

    int t = rand() % 2;
    if (t == 0)
        res.path = path.crossover_points(bot.path, 1).mutation(0.01);
    else
        res.path = path.crossover(bot.path).mutation(0.01);

    return res;
}

Bot Bot::big_mutation() const
{
    Bot res;
    res.path = path.mutation(0.1);
    return res;
}

CompiledBot Bot::compile(int h, int w) const
{
    std::vector<std::pair<int, int> > res_path; // 0 - x+, 1 - x-, 2 - y+, 3 - y-
    res_path.reserve(path.size() + 1);

    int x = 0, y = 0;
    res_path.push_back({0, 0});
    int cmd = -1;

    for (int i = 0; i < path.size(); i++)
    {
        if (i == 0)
        {
            cmd = (path.data[i] / (SimpleDNA::MAX_VALUE / 4)) % 4;
        }
        else
        {
            int ncmd = (path.data[i] / (SimpleDNA::MAX_VALUE / 3)) % 3;
            std::vector<int> possible_steps;
            if (cmd == 0 && ncmd < 1) cmd = ncmd;
            else if (cmd == 0) cmd = ncmd + 1;
            else if (cmd == 1 && ncmd < 0) cmd = ncmd;
            else if (cmd == 1) cmd = ncmd + 1;
            else if (cmd == 2 && ncmd < 3) cmd = ncmd;
            else if (cmd == 2) cmd = ncmd + 1;
            else if (cmd == 3 && ncmd < 2) cmd = ncmd;
            else if (cmd == 3) cmd = ncmd + 1;
        }
        if (cmd == 0) x++;
        else if (cmd == 1) x--;
        else if (cmd == 2) y++;
        else if (cmd == 3) y--;
        else throw std::runtime_error("Bot::compile(): something went wrong");
        res_path.push_back({x, y});
    }

    return CompiledBot(h, w, res_path);
}

std::ostream& operator <<(std::ostream& out, const Bot& bot)
{
    return out << bot.path;
}




CompiledBot::CompiledBot(int height, int width, std::vector<std::pair<int, int> > path) : height(height), width(width), path(path) {}

int CompiledBot::getRepetition() const
{
    std::map<std::pair<int, int>, int> repetitions;

    for (const std::pair<int, int> p: path)
    {
        if (repetitions.count(p) == 0)
            repetitions[p] = 1;
        else
            repetitions[p] += 1;
    }

    int count = 0;
    for (auto it = repetitions.begin(); it != repetitions.end(); it++)
        count += it->second - 1;

    return count;
}

int CompiledBot::getExternals() const
{
    int count = 0;
    for (const std::pair<int, int> p: path)
        count += p.first < 0 || p.second < 0 || p.first >= width || p.second >= height;

    return count;
}

int CompiledBot::getDistanceFrom(int xt, int yt) const
{
    int x = path[path.size() - 1].first, y = path[path.size() - 1].second;
    return std::sqrt((x - xt) * (x - xt) + (y - yt) * (y - yt));
}

cv::Mat CompiledBot::toImage(int scale) const
{
    cv::Mat res = cv::Mat::zeros((height + 1) * scale, (width + 1) * scale, CV_8UC3);
    res = cv::Scalar(255, 255, 255);

    cv::Point2i last_point;

    for (int i = 0; i < path.size(); i++)
    {
        cv::Point2i new_point(path[i].first * scale * 64 + scale * 64, path[i].second * scale * 64 + scale * 64);
        if (i > 0)
        {
            cv::line(res, last_point, new_point, cv::Scalar(0, 0, 0), 3, cv::LINE_AA, 6);
        }
        cv::circle(res, new_point, scale * 64 / 4, cv::Scalar(0, 0, 0), -1, cv::LINE_AA, 6);
        last_point = new_point;
    }

    return res;
}