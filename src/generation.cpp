#include "generation.hpp"

#include <algorithm>


Generation::Generation(EstimateFunc estimate_func) : estimate_func(estimate_func) {}

void Generation::insert_new_bot(const Bot& bot)
{
    bots.push_back({estimate_func(bot), bot});
}

bool compare_first(const std::pair<double, Bot>& a, const std::pair<double, Bot>& b)
{
    return a.first < b.first;
}

void Generation::sort_bots()
{
    sort(bots.begin(), bots.end(), compare_first);
}

Generation Generation::random_generation(int size, EstimateFunc estimate_func, int path_len)
{
    Generation res(estimate_func);

    for (int i = 0; i < size; i++)
        res.insert_new_bot(Bot(path_len));

    res.sort_bots();

    return res;
}

double Generation::get_best_score()
{
    return bots[0].first;
}

Bot Generation::get_best_bot()
{
    return bots[0].second;
}

Generation Generation::net_gen(int size, EstimateFunc estimate_func)
{
    Generation res(estimate_func);

    int t = rand() % 10;

    if (t != 0 || get_best_score() == 0)
    {
        for (int i = 0; i < size / 10; i++)
            res.insert_new_bot(bots[i].second);
    }

    for (int i = res.bots.size(); i < size; i++)
    {
        // std::cout << bots[i].second << std::endl;
        // std::cout << bots[i].second + bots[i].second << std::endl;
        // return res;
        if (t == 0)
            res.insert_new_bot(bots[i].second.big_mutation());
        else
            res.insert_new_bot(bots[rand() % (bots.size() / 5)].second + bots[rand() % bots.size()].second);
    }

    res.sort_bots();
    
    return res;
}