#pragma once

#include "bot.hpp"

#include <vector>


class Generation
{
public:
    typedef std::function<double (const Bot& bot)> EstimateFunc;

private:
    EstimateFunc estimate_func;
    std::vector<std::pair<double, Bot> > bots; // sorted. bots[0] - the best

    void insert_new_bot(const Bot& bot);
    void sort_bots();

    Generation(EstimateFunc estimate_func);

public:
    static Generation random_generation(int size, EstimateFunc estimate_func, int path_len);

    double get_best_score();
    Bot get_best_bot();

    Generation net_gen(int size, EstimateFunc estimate_func);
};