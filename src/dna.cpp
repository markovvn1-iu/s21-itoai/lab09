#include "dna.hpp"

#include <stdexcept>
#include <cstdlib>


SimpleDNA::SimpleDNA() {}

SimpleDNA::SimpleDNA(int len)
{
	data.reserve(len);
	for (int i = 0; i < len; i++)
		data.push_back(rand());
}

SimpleDNA::SimpleDNA(std::vector<uint32_t> data) : data(data) {}

size_t SimpleDNA::size() const
{
	return data.size();
}

SimpleDNA SimpleDNA::mutation(double k) const
{
	std::vector<uint32_t> new_data = data;

	int threshold = RAND_MAX * k;

	for (int i = 0; i < new_data.size(); i++)
		if (rand() < threshold) new_data[i] = rand();

	return SimpleDNA(new_data);
}

SimpleDNA SimpleDNA::mutation_sum(double k, double min_p, double max_p) const
{
	std::vector<uint32_t> new_data = data;

	int threshold = RAND_MAX * k;

	for (int i = 0; i < new_data.size(); i++)
		if (rand() < threshold)
		{
			new_data[i] += rand() * (max_p - min_p) + min_p * SimpleDNA::MAX_VALUE;
			if (new_data[i] > SimpleDNA::MAX_VALUE) new_data[i] -= SimpleDNA::MAX_VALUE;
		}

	return SimpleDNA(new_data);
}

SimpleDNA SimpleDNA::crossover(const SimpleDNA& b) const
{
	std::vector<uint32_t> new_data = data;

	for (int i = 0; i < data.size(); i++)
		if (rand() < SimpleDNA::MAX_VALUE / 2) new_data[i] = b.data[i];

	return SimpleDNA(new_data);
}

SimpleDNA SimpleDNA::crossover_points(const SimpleDNA& b, int max_steps) const
{
	if (max_steps < 1) throw std::runtime_error("SimpleDNA::crossover_points(): max_steps < 1");
	if (size() != b.size()) throw std::runtime_error("SimpleDNA::crossover_points(): size() != b.size(), not implemented");

	if (size() < max_steps) max_steps = size();

	std::vector<uint32_t> new_data = data;

	uint64_t sum = 0;
	std::vector<uint32_t> gap_size;
	gap_size.reserve(max_steps);
	for (int i = 0; i < max_steps; i++)
	{
		uint32_t t = rand();
		sum += t;
		gap_size.push_back(t);
	}

	uint32_t last_split = 0;
	int exchange_on_i = rand() % 2;

	uint64_t csum = 0;
	for (int i = 0; i < max_steps; i++)
	{
		// calculate crossover interval
		csum += gap_size[i];
		uint32_t new_split = csum * size() / sum;
		if (new_split <= last_split) new_split = last_split + 1;
		if (new_split > size()) break;

		// exchange interval [last_split; new_split)
		if (i % 2 == exchange_on_i)
		{
			for (int j = last_split; j < new_split; j++)
				new_data[j] = b.data[j];
		}

		last_split = new_split;
	}

	if (last_split != size()) throw std::runtime_error("SimpleDNA::crossover(): something goes wrong");

	return SimpleDNA(new_data);
}

std::ostream& operator <<(std::ostream& out, const SimpleDNA& dna)
{
	out << dna.size() << ' ';

	for (int i = 0; i < dna.size(); i++)
	{
		if (i != 0) out << ' ';
		out << dna.data[i];
	}

	return out;
}
