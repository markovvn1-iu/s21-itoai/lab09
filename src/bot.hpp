#pragma once

#include "dna.hpp"

#include <vector>
#include <ostream>

#include <opencv2/opencv.hpp>


class CompiledBot;

class Bot
{
private:
	SimpleDNA path;

public:
	Bot();
	Bot(int path_len);

	Bot operator +(const Bot& bot) const;
	Bot big_mutation() const;

	CompiledBot compile(int h, int w) const;

	friend std::ostream& operator <<(std::ostream& out, const Bot& bot);
};


class CompiledBot
{
friend Bot;

private:
	int height, width;
	std::vector<std::pair<int, int> > path;  // 0 - x+, 1 - x-, 2 - y+, 3 - y-

	CompiledBot(int height, int width, std::vector<std::pair<int, int> > path);

public:
	int getRepetition() const;
	int getExternals() const;
	int getDistanceFrom(int xt, int yt) const;
	cv::Mat toImage(int scale) const;
};